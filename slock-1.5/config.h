/* user and group to drop privileges to */
static const char *user  = "patrik";
static const char *group = "patrik";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#005577",   /* during input */
	[FAILED] = "#CC3333",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* default message */
static const char * message = "Linux & Tea <3";

/* text color */
static const char * text_color = "#000000";

/* text size (must be a valid size) */
static const char * font_name = "12x24";

/* time in seconds before the monitor shuts down */
static const int monitortime = 10;

/* Background image path, should be available to the user above */
static const char* background_image = "/home/patrik/wall.jpg";
